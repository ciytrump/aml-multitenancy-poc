package com.ascendcorp.aml;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReactiveElsSpringbootApp {
  public static void main(String[] args) {
    SpringApplication.run(ReactiveElsSpringbootApp.class, args);
  }
}
