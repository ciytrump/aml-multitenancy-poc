package com.ascendcorp.aml.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;

@Document(indexName = "customer")
@Data
public class CustomerEntity {

  @Id private String id;

  @Field("name")
  private String name;

  @Field("age")
  private Integer age;
}
