package com.ascendcorp.aml.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorCode {
  SUCCESS("0000", "Success"),
  ERROR_COMMON_INVALID_REQUEST("4000", "Invalid request"),
  ERROR_INVALID_REQUEST("4001", "Parameter: %s invalid"),
  INVALID_INCIDENT_STATUS("4002", "Invalid incident status"),
  ACCESS_DENIED("4031", "Access denied"),
  EXCEED_ALLOWED_INCIDENT_ATTACHMENT("4037", "Maximum: [%s] attachments. Incident: [%s] has exceed attachments"),
  NOT_ASSIGNEE_INCIDENT("4038", "Only assignee is allowed the action"),
  INCIDENT_NOT_FOUND("4041", "Incident with id [%s] is not found"),
  ATTACHMENT_NOT_FOUND("4042", "Attachment with id [%s] is not found"),
  ACTION_NOT_ALLOW_CLOSED_ALERT("4003", "Action is not allowed for closed alert"),
  ACTION_NOT_ALLOW_UNASSIGNED_ALERT("4005", "Action is not allowed for unassigned alert"),
  WORK_FLOW_STAGE_INCORRECT("4004", "Next stage is not correct"),
  ENTITY_NOT_FOUND("4044", "Entity with id [%s] is not found"),
  DATASOURCE_UNSUPPORTED("4045", "The datasource has unsupported for country: [%s]"),
  GENERAL_ERROR("5000", "General error");

  private String code;
  private String message;

  public String valueAsString(Object[] objects) {
    StringBuilder sb = new StringBuilder().append(code).append(" - ").append(message);
    return String.format(sb.toString(), objects);
  }

  @Override
  public String toString() {
    return "ErrorCode{" + "code='" + code + '\'' + ", message='" + message + '\'' + '}';
  }
}
