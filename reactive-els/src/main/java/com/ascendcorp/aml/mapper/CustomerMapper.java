package com.ascendcorp.aml.mapper;

import com.ascendcorp.aml.dto.CustomerDto;
import com.ascendcorp.aml.entity.CustomerEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper {

  CustomerDto toDto(CustomerEntity entity);

  CustomerEntity toEntity(CustomerDto dto);
}
