package com.ascendcorp.aml.service;

import java.util.function.Function;
import lombok.Data;
import reactor.core.publisher.Mono;
import reactor.util.context.Context;

@Data
public class TenantIdHolder {

  public static final String TENANT_ID = TenantIdHolder.class.getName() + ".TENANT_ID";

  public static Context withTenantId(String id) {
    return Context.of(TENANT_ID, Mono.just(id));
  }

  public static Mono<Object> getTenantId() {
    return Mono.deferWithContext(
        context -> {
          if (context.hasKey(TENANT_ID)) {
            return context.get(TENANT_ID);
          }
          return Mono.empty();
        });
  }

  public static Function<Context, Context> clearContext() {
    return context -> context.delete(TENANT_ID);
  }
}
