package com.ascendcorp.aml.service;

import com.ascendcorp.aml.dto.CustomerDto;
import reactor.core.publisher.Mono;

public interface CustomerService {

  Mono<CustomerDto> createCustomer(CustomerDto request);

  Mono<CustomerDto> getCustomer(String id);

  Mono<Integer> deleteCustomer(String id);
}
