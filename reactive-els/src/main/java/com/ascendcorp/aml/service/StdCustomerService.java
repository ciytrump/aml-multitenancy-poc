package com.ascendcorp.aml.service;

import com.ascendcorp.aml.dto.CustomerDto;
import com.ascendcorp.aml.entity.CustomerEntity;
import com.ascendcorp.aml.exception.BusinessException;
import com.ascendcorp.aml.exception.ErrorCode;
import com.ascendcorp.aml.mapper.CustomerMapper;
import com.ascendcorp.aml.repository.CustomerRepository;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@Slf4j
@RequiredArgsConstructor
public class StdCustomerService implements CustomerService {

  private final CustomerRepository customerRepository;

  private final CustomerMapper mapper;

  @Override
  public Mono<CustomerDto> createCustomer(CustomerDto request) {
    log.info("method: createCustomer, request: {}", request);

    CustomerEntity entity = mapper.toEntity(request);
    entity.setId(null);

    return customerRepository.save(entity).map(mapper::toDto);
  }

  @Override
  public Mono<CustomerDto> getCustomer(String id) {
    log.info("method: getCustomer, id: {}", id);
    return customerRepository
        .findById(id)
        .filter(
            it -> {
              if (Objects.isNull(it)) {
                throw new BusinessException(ErrorCode.ENTITY_NOT_FOUND, new Object[] {id});
              }
              return true;
            })
        .map(mapper::toDto);
  }

  @Override
  public Mono<Integer> deleteCustomer(String id) {
    log.info("method: deleteCustomer, id: {}", id);
    return customerRepository
        .findById(id)
        .filter(
            it -> {
              if (Objects.isNull(it)) {
                throw new BusinessException(ErrorCode.ENTITY_NOT_FOUND, new Object[] {id});
              }
              return true;
            })
        .flatMap(customerRepository::delete)
        .thenReturn(1);
  }
}
