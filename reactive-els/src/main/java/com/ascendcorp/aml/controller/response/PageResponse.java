package com.ascendcorp.aml.controller.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonNaming(SnakeCaseStrategy.class)
@AllArgsConstructor
@NoArgsConstructor
public class PageResponse {

  private Integer totalPages;

  private Boolean hasNext;

  private Boolean hasPrevious;

  private Integer currentPage;

  private Long totalElements;

}
