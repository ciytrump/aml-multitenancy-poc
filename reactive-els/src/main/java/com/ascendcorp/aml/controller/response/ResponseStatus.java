package com.ascendcorp.aml.controller.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonNaming(SnakeCaseStrategy.class)
@NoArgsConstructor
@AllArgsConstructor
public class ResponseStatus implements Serializable {

  private static final long serialVersionUID = 1L;

  private String code;

  private String message;

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("{");
    sb.append("\"code\":\"").append(code).append("\"");
    sb.append(", \"message\":\"").append(message).append("\"");
    sb.append('}');
    return sb.toString();
  }
}
