package com.ascendcorp.aml.controller;

import com.ascendcorp.aml.controller.response.GeneralResponse;
import com.ascendcorp.aml.controller.response.ResponseFactory;
import com.ascendcorp.aml.dto.CustomerDto;
import com.ascendcorp.aml.dto.UserDto;
import com.ascendcorp.aml.kafka.KafkaChannel;
import com.ascendcorp.aml.service.CustomerService;
import java.util.Locale;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class CustomerController {

  private final CustomerService customerService;

  private final ResponseFactory responseFactory;

  private final KafkaChannel kafkaChannel;

  @PostMapping("/customers")
  public Mono<GeneralResponse<CustomerDto>> createCustomer(
      @RequestBody CustomerDto request,
      @RequestHeader(value = "country_code", required = false) String countryCode,
      Locale locale) {
    return customerService.createCustomer(request).map(it -> responseFactory.success(it, locale));
    //    return responseFactory.success(, locale);
  }

  @GetMapping("/customers/{id}")
  public Mono<GeneralResponse<CustomerDto>> getCustomer(
      @PathVariable String id,
      @RequestHeader(value = "country_code", required = false) String countryCode,
      Locale locale) {
    return customerService.getCustomer(id).map(it -> responseFactory.success(it, locale));
  }

  @DeleteMapping("/customers/{id}")
  public Mono<GeneralResponse<Object>> deleteCustomer(
      @PathVariable String id,
      @RequestHeader(value = "country_code", required = false) String countryCode,
      Locale locale) {
    return customerService.deleteCustomer(id).map(it -> responseFactory.success(locale));
  }

  @GetMapping("/customers/push-message/{num}")
  public Mono<GeneralResponse<Object>> pushMessage(
      @PathVariable(value = "num", required = true) Integer num,
      @RequestHeader(value = "country_code", required = false) String countryCode,
      Locale locale) {

    for (int i = 0; i < num; i++) {
      kafkaChannel
          .output()
          .send(
              MessageBuilder.withPayload(
                      new UserDto().setName(RandomStringUtils.random(5, true, true) + "_" + i))
                  .build());
    }

    return Mono.just(responseFactory.success(locale));
  }
}
