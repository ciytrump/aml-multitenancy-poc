package com.ascendcorp.aml.dto;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Field;

@Data
public class CustomerDto {

  private String id;

  private String name;

  private Integer age;
}
