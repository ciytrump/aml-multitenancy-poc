package com.ascendcorp.aml.kafka;

import com.ascendcorp.aml.dto.UserDto;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
@Slf4j
public class KafkaConsumer {

  @StreamListener(KafkaChannel.INPUT)
  public void onMessage(List<UserDto> messages){
    log.info("method: onMessage, messages: {}", messages);
    Mono.just(messages).subscribe();
  }

}
