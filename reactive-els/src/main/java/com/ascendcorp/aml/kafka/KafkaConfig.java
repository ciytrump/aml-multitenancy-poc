package com.ascendcorp.aml.kafka;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding(KafkaChannel.class)
public class KafkaConfig {

}
