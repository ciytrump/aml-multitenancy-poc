package com.ascendcorp.aml.kafka;

import org.reactivestreams.Subscription;
import reactor.core.CoreSubscriber;

public class MySubscriber implements CoreSubscriber {

  private int n = 10;

  @Override
  public void onSubscribe(Subscription s) {
    s.request(n);
  }

  @Override
  public void onNext(Object o) {

  }

  @Override
  public void onError(Throwable throwable) {

  }

  @Override
  public void onComplete() {

  }
}
