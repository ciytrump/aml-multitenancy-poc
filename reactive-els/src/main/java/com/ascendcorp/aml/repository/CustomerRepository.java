package com.ascendcorp.aml.repository;

import com.ascendcorp.aml.entity.CustomerEntity;
import org.springframework.data.elasticsearch.repository.ReactiveElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository
    extends ReactiveElasticsearchRepository<CustomerEntity, String> {}
