package com.ascendcorp.aml.filter;

import com.ascendcorp.aml.service.TenantIdHolder;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class TenantFilter implements Filter {

  public static final String COUNTRY_CODE_HEADER = "country_code";

  @Override
  public void doFilter(
      ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
      throws IOException, ServletException {
    try {
      HttpServletRequest req = (HttpServletRequest) servletRequest;
      String countryCode = req.getHeader(COUNTRY_CODE_HEADER);
      if (StringUtils.isNotBlank(countryCode)) {
        TenantIdHolder.withTenantId(countryCode.toLowerCase());
      }
      filterChain.doFilter(servletRequest, servletResponse);
    } finally {
      TenantIdHolder.clearTenantId();
    }
  }
}
