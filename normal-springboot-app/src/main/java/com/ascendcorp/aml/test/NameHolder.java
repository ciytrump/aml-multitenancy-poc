package com.ascendcorp.aml.test;

import com.alibaba.ttl.TransmittableThreadLocal;

public class NameHolder {

  private static final ThreadLocal<String> context = new TransmittableThreadLocal<>();

  public static void withName(String name) {
    context.set(name);
  }

  public static String getName() {
    return context.get();
  }

  public static void remove() {
    context.remove();
  }
}
