package com.ascendcorp.aml.test;

import java.util.Random;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class User {

  private String name;

  private Integer age;


}
