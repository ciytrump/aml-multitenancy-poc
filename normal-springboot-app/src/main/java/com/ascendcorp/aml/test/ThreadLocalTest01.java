package com.ascendcorp.aml.test;

import com.alibaba.ttl.threadpool.TtlExecutors;
import com.ascendcorp.aml.config.executor.ThreadLocalAwareThreadPool;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ThreadLocalTest01 {

  public static void main(String[] args) {

    NameHolder.withName("VNM");

    log.info("first value: {}", NameHolder.getName());

    Runnable runnable = () -> {
      log.info("value in child thread: {}", NameHolder.getName());
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    };


    ThreadPoolExecutor executor = new ThreadLocalAwareThreadPool(1, 1,
        0L, TimeUnit.MILLISECONDS,
        new LinkedBlockingQueue<Runnable>());


    ExecutorService ttlExecutorService = TtlExecutors.getTtlExecutorService(executor);

    ttlExecutorService.submit(runnable);

    NameHolder.withName("THA");

    ttlExecutorService.submit(runnable);


    log.info("final value: {}", NameHolder.getName());

    executor.shutdown();


  }


}
