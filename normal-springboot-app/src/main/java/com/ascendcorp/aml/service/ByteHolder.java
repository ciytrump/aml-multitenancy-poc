package com.ascendcorp.aml.service;

import com.alibaba.ttl.TransmittableThreadLocal;

public class ByteHolder {

  private static ThreadLocal<byte[]> context = new TransmittableThreadLocal<>();

  public static void withByte(byte[] bytes) {
    context.set(bytes);
  }

  public static byte[] getBytes() {
    return context.get();
  }

  public static void clear() {
    context.remove();
  }
}
