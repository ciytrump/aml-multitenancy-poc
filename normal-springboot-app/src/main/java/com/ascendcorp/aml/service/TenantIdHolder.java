package com.ascendcorp.aml.service;

import com.alibaba.ttl.TransmittableThreadLocal;

public class TenantIdHolder {
  private static final ThreadLocal<String> CONTEXT = new TransmittableThreadLocal<>();

  public static String getTenantId() {
    return CONTEXT.get();
  }

  public static void withTenantId(String tenantId) {
    CONTEXT.set(tenantId);
  }

  public static void clearTenantId() {
    CONTEXT.remove();
  }
}
