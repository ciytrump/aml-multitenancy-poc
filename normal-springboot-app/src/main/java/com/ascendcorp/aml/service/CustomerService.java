package com.ascendcorp.aml.service;

import com.ascendcorp.aml.dto.CustomerDto;
import java.util.List;

public interface CustomerService {

  Long createCustomer(CustomerDto request);

  CustomerDto getCustomer(Long id);

  void deleteCustomer(Long id);
}
