package com.ascendcorp.aml.service;

import com.ascendcorp.aml.entity.CustomerEntity;
import com.ascendcorp.aml.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.binder.kafka.BinderHeaderMapper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class AsyncCustomerService {

  private final CustomerRepository customerRepository;

  @Async("alibabaExecutorService")
//  @Async("executorService")
  public Long createCustomer(CustomerEntity entity){
    log.info("method: createCustomer");
    customerRepository.save(entity);
    return entity.getId();
  }

}
