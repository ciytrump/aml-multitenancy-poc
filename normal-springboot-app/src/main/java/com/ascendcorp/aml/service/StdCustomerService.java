package com.ascendcorp.aml.service;

import com.ascendcorp.aml.dto.CustomerDto;
import com.ascendcorp.aml.entity.CustomerEntity;
import com.ascendcorp.aml.exception.BusinessException;
import com.ascendcorp.aml.exception.ErrorCode;
import com.ascendcorp.aml.kafka.KafkaChannel;
import com.ascendcorp.aml.mapper.CustomerMapper;
import com.ascendcorp.aml.repository.CustomerRepository;
import java.util.Date;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Service
@Slf4j
@RequiredArgsConstructor
public class StdCustomerService implements CustomerService {

  private final CustomerRepository customerRepository;

  private final CustomerMapper mapper;

  private final AsyncCustomerService asyncCustomerService;

  private int count = 0;

  private final KafkaChannel kafkaChannel;

  //  @Override
  //  @Transactional
  //  public Long createCustomer(CustomerDto request) {
  //    log.info("method: createCustomer, request: {}", request);
  //    CustomerEntity entity = mapper.toEntity(request);
  //    entity.setId(null);
  //    entity.setCreatedAt(new Date());
  //    customerRepository.save(entity);
  //
  ////    count++;
  ////    if (count % 2 == 0) {
  ////      log.error("method: createCustomer, test rollback transaction, count: {}", count);
  ////      throw new BusinessException(ErrorCode.ERROR_COMMON_INVALID_REQUEST);
  ////    }
  //
  //    CustomerDto customerDto = getCustomer(entity.getId());
  //    log.info("method: createCustomer, {}", customerDto);
  //
  //    return entity.getId();
  //  }

  @Override
  @Transactional
  public Long createCustomer(CustomerDto request) {
    log.info("method: createCustomer, request: {}", request);
    CustomerEntity entity = mapper.toEntity(request);
    entity.setId(null);
    entity.setCreatedAt(new Date());
    asyncCustomerService.createCustomer(entity);

    Mono.just(1)
        .publishOn(Schedulers.boundedElastic())
        .doOnNext(
            it -> {
              log.info("---> doOnNext: {}", it);
              Mono.just(it)
                  .subscribe(
                      a -> {
                        log.info("----> Mono2: {}", a);
                      });
            })
        .subscribe(
            it -> {
              log.info("---> mono test: {}", it);
            });

    kafkaChannel
        .output()
        .send(MessageBuilder.withPayload(RandomStringUtils.random(5, true, true)).build());

    return entity.getId();
  }

  @Override
  @Transactional
  @Cacheable(value = "customer", keyGenerator = "customKeyGenerator")
  public CustomerDto getCustomer(Long id) {
    log.info("method: getCustomer, id: {}", id);
    CustomerEntity entity =
        customerRepository
            .findById(id)
            .orElseThrow(
                () -> new BusinessException(ErrorCode.ENTITY_NOT_FOUND, new Object[] {id}));
    return mapper.toDto(entity);
  }

  @Override
  public void deleteCustomer(Long id) {
    log.info("method: deleteCustomer, id: {}", id);
    CustomerEntity entity =
        customerRepository
            .findById(id)
            .orElseThrow(
                () -> new BusinessException(ErrorCode.ENTITY_NOT_FOUND, new Object[] {id}));
    customerRepository.delete(entity);
  }
}
