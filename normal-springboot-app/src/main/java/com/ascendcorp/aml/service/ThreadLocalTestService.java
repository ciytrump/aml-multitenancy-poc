package com.ascendcorp.aml.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ThreadLocalTestService {

  private final AsyncThreadLocalTestService asyncThreadLocalTestService;

  public void wasteThreadLocalInstance() {
   ByteHolder.withByte(new byte[1024*1024*5]);
   asyncThreadLocalTestService.wasteThreadLocal();
  }

}
