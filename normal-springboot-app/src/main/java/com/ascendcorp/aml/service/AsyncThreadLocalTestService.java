package com.ascendcorp.aml.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncThreadLocalTestService {


  @Async("alibabaExecutorService1")
  public void wasteThreadLocal(){
      ByteHolder.getBytes();
  }

}
