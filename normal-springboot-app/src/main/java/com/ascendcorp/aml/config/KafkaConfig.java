package com.ascendcorp.aml.config;

import com.ascendcorp.aml.kafka.KafkaChannel;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBinding(KafkaChannel.class)
public class KafkaConfig {}
