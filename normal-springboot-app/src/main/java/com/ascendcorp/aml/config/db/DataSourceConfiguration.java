package com.ascendcorp.aml.config.db;

import lombok.Data;

@Data
public class DataSourceConfiguration {

  private String jdbcUrl;

  private String username;

  private String password;

  private String driverClassName;

  private String maximumPoolSize;
}
