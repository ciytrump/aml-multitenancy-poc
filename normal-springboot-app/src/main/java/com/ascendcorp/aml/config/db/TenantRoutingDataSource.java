package com.ascendcorp.aml.config.db;

import com.ascendcorp.aml.exception.BusinessException;
import com.ascendcorp.aml.exception.ErrorCode;
import com.ascendcorp.aml.service.TenantIdHolder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

@Slf4j
@RequiredArgsConstructor
public class TenantRoutingDataSource extends AbstractRoutingDataSource {

  public static final String DEFAULT_TENANCY_WHILE_STARTING_UP_APP = "vnm";

  public static Boolean IS_APP_STARTUP = false;

  private final DataSourceProperties dataSourceProperties;

  @Override
  protected Object determineCurrentLookupKey() {

    if (!IS_APP_STARTUP) {
      log.info(
          "method: determineCurrentLookupKey, set default tenancy for key while starting up app: {}",
          DEFAULT_TENANCY_WHILE_STARTING_UP_APP);
      return DEFAULT_TENANCY_WHILE_STARTING_UP_APP;
    }

    String tenantId = TenantIdHolder.getTenantId();
    log.info("method: determineCurrentLookupKey, lookup datasource for tenantId: {}", tenantId);
    if (!dataSourceProperties.isConfigurationSupported(tenantId)) {
      log.error(
          "method: determineCurrentLookupKey, the datasource has not supported for tenantId: {}",
          tenantId);
      throw new BusinessException(ErrorCode.DATASOURCE_UNSUPPORTED, new Object[] {tenantId});
    }
    return tenantId;
  }
}
