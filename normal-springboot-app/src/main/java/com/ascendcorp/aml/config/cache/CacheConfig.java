package com.ascendcorp.aml.config.cache;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class CacheConfig {

  @Bean("customKeyGenerator")
  public CustomKeyGenerator keyGenerator() {
    return new CustomKeyGenerator();
  }
}
