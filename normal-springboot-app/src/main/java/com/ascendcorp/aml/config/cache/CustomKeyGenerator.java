package com.ascendcorp.aml.config.cache;

import com.ascendcorp.aml.service.TenantIdHolder;
import java.lang.reflect.Method;
import org.springframework.cache.interceptor.SimpleKeyGenerator;

public class CustomKeyGenerator extends SimpleKeyGenerator {

  @Override
  public Object generate(Object target, Method method, Object... params) {

    return TenantIdHolder.getTenantId() + "_" + generateKey(params);
  }
}
