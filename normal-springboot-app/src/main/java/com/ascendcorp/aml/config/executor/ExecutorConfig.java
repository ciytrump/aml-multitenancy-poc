package com.ascendcorp.aml.config.executor;

import com.alibaba.ttl.threadpool.TtlExecutors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExecutorConfig {

  @Bean("alibabaExecutorService")
  public ExecutorService executorService() {
    return TtlExecutors.getTtlExecutorService(threadPoolExecutor());
  }

  @Bean("executorService")
  public ThreadPoolExecutor threadPoolExecutor() {
    return new ThreadLocalAwareThreadPool(
        1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
  }

  @Bean("alibabaExecutorService1")
  public ExecutorService executorService1() {
    return TtlExecutors.getTtlExecutorService(threadPoolExecutor1());
  }

  @Bean("executorService1")
  public ThreadPoolExecutor threadPoolExecutor1() {
    return new ThreadLocalAwareThreadPool(
        20, 20, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<>());
  }
}
