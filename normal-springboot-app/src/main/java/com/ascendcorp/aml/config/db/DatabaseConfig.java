package com.ascendcorp.aml.config.db;

import com.zaxxer.hikari.HikariDataSource;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableConfigurationProperties(DataSourceProperties.class)
@RequiredArgsConstructor
@EnableTransactionManagement
public class DatabaseConfig {

  private final DataSourceProperties dataSourceProperties;

  @Bean
  public DataSource dataSource() {
    TenantRoutingDataSource dataSource = new TenantRoutingDataSource(dataSourceProperties);
    dataSource.setTargetDataSources(createTargetDataSources());
    return dataSource;
  }

  private Map<Object, Object> createTargetDataSources() {
    Map<Object, Object> result = new HashMap<>();
    dataSourceProperties
        .getConfigurations()
        .forEach((key, value) -> result.put(key, createDataSource(key, value)));
    return result;
  }

  private DataSource createDataSource(String key, DataSourceConfiguration dsConfig) {
    HikariDataSource hdataSource = new HikariDataSource();
    hdataSource.setDriverClassName(dsConfig.getDriverClassName());
    hdataSource.setJdbcUrl(dsConfig.getJdbcUrl());
    hdataSource.setUsername(dsConfig.getUsername());
    hdataSource.setPassword(dsConfig.getPassword());
    hdataSource.setCatalog("*****");
    hdataSource.setPoolName(key + "_HIKARICP_POOL");
    return hdataSource;
  }

  @EventListener(ApplicationReadyEvent.class)
  public void markApplicationReady() {
    TenantRoutingDataSource.IS_APP_STARTUP = true;
  }
}
