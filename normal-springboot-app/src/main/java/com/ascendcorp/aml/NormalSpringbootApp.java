package com.ascendcorp.aml;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NormalSpringbootApp {

  public static void main(String[] args) {
    SpringApplication.run(NormalSpringbootApp.class, args);
  }
}
