package com.ascendcorp.aml.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Component
@Slf4j
@RequiredArgsConstructor
public class KafkaConsumer {

  @StreamListener(target = KafkaChannel.INPUT)
  public void onMessage(
      @Payload String data, @Header(KafkaHeaders.ACKNOWLEDGMENT) Acknowledgment acknowledgment) {
    log.info("method: onMessage, data: {}", data);

    Mono.just(data)
        .publishOn(Schedulers.boundedElastic())
        .doOnNext(
            it -> {
              log.info("---> doOnNext: {}", it);
              Mono.just(it)
                  .publishOn(Schedulers.parallel())
                  .subscribe(
                      a -> {
                        log.info("----> Mono2: {}", a);
                      });
            })
        .subscribe(
            it -> {
              log.info("---> mono test: {}", it);
            });

    acknowledgment.acknowledge();
  }
}
