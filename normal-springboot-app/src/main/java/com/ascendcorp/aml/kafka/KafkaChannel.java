package com.ascendcorp.aml.kafka;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface KafkaChannel {

  String INPUT = "input1";

  String OUTPUT = "output1";

  @Input(KafkaChannel.INPUT)
  SubscribableChannel input();

  @Output(KafkaChannel.OUTPUT)
  MessageChannel output();
}
