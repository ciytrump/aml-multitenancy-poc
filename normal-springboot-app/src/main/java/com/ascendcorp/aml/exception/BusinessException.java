package com.ascendcorp.aml.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class BusinessException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private ErrorCode errorCode;

  private Object[] objects;

  public BusinessException(ErrorCode errorCode) {
    super(errorCode.valueAsString(null));
    this.errorCode = errorCode;
  }

  public BusinessException(ErrorCode errorCode, Object[] objects) {
    super(errorCode.valueAsString(objects));
    this.errorCode = errorCode;
    this.objects = objects;
  }
}
