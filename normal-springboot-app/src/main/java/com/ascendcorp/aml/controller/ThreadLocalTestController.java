package com.ascendcorp.aml.controller;

import com.ascendcorp.aml.controller.response.GeneralResponse;
import com.ascendcorp.aml.controller.response.ResponseFactory;
import com.ascendcorp.aml.service.ThreadLocalTestService;
import java.util.Locale;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ThreadLocalTestController {

  private final ThreadLocalTestService threadLocalTestService;

  private final ResponseFactory responseFactory;

  @GetMapping("/thread-local/waste")
  public GeneralResponse<Object> wasteThreadLocal(Locale locale) {
    threadLocalTestService.wasteThreadLocalInstance();
    return responseFactory.success(locale);
  }
}
