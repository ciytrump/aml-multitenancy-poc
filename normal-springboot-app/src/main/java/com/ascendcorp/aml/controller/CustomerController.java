package com.ascendcorp.aml.controller;

import com.ascendcorp.aml.controller.response.GeneralResponse;
import com.ascendcorp.aml.controller.response.ResponseFactory;
import com.ascendcorp.aml.dto.CustomerDto;
import com.ascendcorp.aml.service.CustomerService;
import java.util.Locale;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class CustomerController {

  private final CustomerService customerService;

  private final ResponseFactory responseFactory;

  @PostMapping("/customers")
  public GeneralResponse<Long> createCustomer(
      @RequestBody CustomerDto request,
      @RequestHeader(value = "country_code", required = false) String countryCode,
      Locale locale) {
    return responseFactory.success(customerService.createCustomer(request), locale);
  }

  @GetMapping("/customers/{id}")
  public GeneralResponse<CustomerDto> getCustomer(
      @PathVariable Long id, @RequestHeader(value = "country_code", required = false) String countryCode, Locale locale) {
    return responseFactory.success(customerService.getCustomer(id), locale);
  }

  @DeleteMapping("/customers/{id}")
  public GeneralResponse<Object> deleteCustomer(
      @PathVariable Long id, @RequestHeader(value = "country_code", required = false) String countryCode, Locale locale) {

    customerService.deleteCustomer(id);
    return responseFactory.success(locale);
  }
}
