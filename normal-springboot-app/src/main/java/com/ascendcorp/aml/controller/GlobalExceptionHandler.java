package com.ascendcorp.aml.controller;

import com.ascendcorp.aml.controller.response.GeneralResponse;
import com.ascendcorp.aml.controller.response.ResponseFactory;
import com.ascendcorp.aml.exception.BusinessException;
import com.ascendcorp.aml.exception.ErrorCode;
import java.util.Locale;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.CannotCreateTransactionException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RequiredArgsConstructor
@RestControllerAdvice
public class GlobalExceptionHandler {

  private final ResponseFactory responseFactory;

  @ExceptionHandler(Exception.class)
  @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
  public GeneralResponse<Object> handleException(Exception ex, Locale locale) {
    log.error("method: handleException, ex: ", ex);
    return responseFactory.error(ErrorCode.GENERAL_ERROR, null, locale);
  }

  @ExceptionHandler(CannotCreateTransactionException.class)
  @ResponseStatus(code = HttpStatus.BAD_REQUEST)
  public GeneralResponse<Object> handleCannotCreateTransactionException(
      CannotCreateTransactionException ex, Locale locale) {
    log.error("method: handleCannotCreateTransactionException, ex: {}", ex.getMessage());
    Throwable throwable = ex.getCause();
    if (throwable instanceof BusinessException) {
      BusinessException businessException = (BusinessException) throwable;
      return responseFactory.error(
          businessException.getErrorCode(), businessException.getObjects(), locale);
    }
    return responseFactory.error(ErrorCode.GENERAL_ERROR, null, locale);
  }

  @ExceptionHandler(BusinessException.class)
  @ResponseStatus(code = HttpStatus.BAD_REQUEST)
  public GeneralResponse<Object> handleBusinessException(BusinessException ex, Locale locale) {
    log.error("method: handleBusinessException, ex: {}", ex.getMessage());
    return responseFactory.error(ex.getErrorCode(), ex.getObjects(), locale);
  }

  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(code = HttpStatus.BAD_REQUEST)
  public GeneralResponse<Object> handleMethodArgumentNotValidException(
      MethodArgumentNotValidException ex, Locale locale) {
    log.error("method: handleMethodArgumentNotValidException, ex: {}", ex.getMessage());
    return responseFactory.error(
        ErrorCode.ERROR_INVALID_REQUEST,
        ex.getBindingResult().getAllErrors().get(0).getDefaultMessage(),
        null,
        locale);
  }
}
