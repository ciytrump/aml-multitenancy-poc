create table if not exists customer
(
    id bigserial not null
        constraint customer_pkey
            primary key,
    age integer,
    created_at timestamp,
    name varchar(255),
    request_id varchar(255)
);
