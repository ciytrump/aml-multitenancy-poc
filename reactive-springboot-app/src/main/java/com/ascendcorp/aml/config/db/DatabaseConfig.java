package com.ascendcorp.aml.config.db;

import io.r2dbc.pool.ConnectionPool;
import io.r2dbc.pool.ConnectionPoolConfiguration;
import io.r2dbc.spi.ConnectionFactories;
import io.r2dbc.spi.ConnectionFactory;
import io.r2dbc.spi.ConnectionFactoryOptions;
import io.r2dbc.spi.Option;
import java.util.HashMap;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableConfigurationProperties(DataSourceProperties.class)
@RequiredArgsConstructor
@EnableTransactionManagement
public class DatabaseConfig {

  private final DataSourceProperties dataSourceProperties;

  @Bean
  public ConnectionFactory dataSource() {
    TenantRoutingConnectionFactory connectionFactory =
        new TenantRoutingConnectionFactory(dataSourceProperties);
    connectionFactory.setTargetConnectionFactories(createTargetConnectionFactory());
    connectionFactory.setDefaultTargetConnectionFactory(defaultTargetConnectionFactory());
    return connectionFactory;
  }

  private Object defaultTargetConnectionFactory() {
    DataSourceConfiguration dsConfig =
        dataSourceProperties
            .getConfigurations()
            .get(TenantRoutingConnectionFactory.DEFAULT_TENANCY_WHILE_STARTING_UP_APP);
    return createTargetConnectionFactory(
        TenantRoutingConnectionFactory.DEFAULT_TENANCY_WHILE_STARTING_UP_APP, dsConfig);
  }

  private Map<String, ConnectionFactory> createTargetConnectionFactory() {
    Map<String, ConnectionFactory> result = new HashMap<>();
    dataSourceProperties
        .getConfigurations()
        .forEach((key, value) -> result.put(key, createTargetConnectionFactory(key, value)));
    return result;
  }

  private ConnectionFactory createTargetConnectionFactory(
      String key, DataSourceConfiguration dsConfig) {

    ConnectionFactory connectionFactory =
        ConnectionFactories.get(
            ConnectionFactoryOptions.builder()
                .option(ConnectionFactoryOptions.DRIVER, dsConfig.getDatabaseType())
                .option(ConnectionFactoryOptions.HOST, dsConfig.getHost())
                .option(ConnectionFactoryOptions.PORT, dsConfig.getPort())
                .option(ConnectionFactoryOptions.USER, dsConfig.getUsername())
                .option(ConnectionFactoryOptions.PASSWORD, dsConfig.getPassword())
                .option(ConnectionFactoryOptions.DATABASE, dsConfig.getDatabase())
                .option(Option.valueOf("schema"), dsConfig.getSchema())
                .build());

    ConnectionPoolConfiguration configuration =
        ConnectionPoolConfiguration.builder(connectionFactory)
            .maxSize(Integer.valueOf(dsConfig.getMaximumPoolSize()))
            .name(key + "_R2DBC_POOL")
            .build();

    ConnectionPool connectionPool = new ConnectionPool(configuration);

    return connectionPool;
  }
}
