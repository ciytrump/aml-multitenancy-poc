package com.ascendcorp.aml.config.db;

import com.ascendcorp.aml.exception.BusinessException;
import com.ascendcorp.aml.exception.ErrorCode;
import com.ascendcorp.aml.service.TenantIdHolder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.r2dbc.connectionfactory.lookup.AbstractRoutingConnectionFactory;
import reactor.core.publisher.Mono;

@Slf4j
@RequiredArgsConstructor
public class TenantRoutingConnectionFactory extends AbstractRoutingConnectionFactory {

  public static final String DEFAULT_TENANCY_WHILE_STARTING_UP_APP = "vnm";

  private final DataSourceProperties dataSourceProperties;

  @Override
  protected Mono<Object> determineCurrentLookupKey() {

    return TenantIdHolder.getTenantId()
        .doOnNext(
            tenantId -> {
              log.info(
                  "method: determineCurrentLookupKey, lookup datasource for tenantId:{}", tenantId);
              if (!dataSourceProperties.isConfigurationSupported(tenantId.toString())) {
                log.error(
                    "method: determineCurrentLookupKey, the datasource has not supported for tenantId: {}",
                    tenantId);
                throw new BusinessException(
                    ErrorCode.DATASOURCE_UNSUPPORTED, new Object[] {tenantId.toString()});
              }
            })
        .switchIfEmpty(
            Mono.error(
                () -> {
                  log.error("method: determineCurrentLookupKey, no datasource is provided");
                  return new BusinessException(ErrorCode.DATASOURCE_UNSUPPORTED, null);
                }));
  }
}
