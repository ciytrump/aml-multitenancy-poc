package com.ascendcorp.aml.config.cache;

import com.google.common.cache.CacheBuilder;
import java.util.concurrent.TimeUnit;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class CacheConfig {

  @Bean("customKeyGenerator")
  public CustomKeyGenerator keyGenerator() {
    return new CustomKeyGenerator();
  }


  @Bean("test01")
  public CacheManager cacheManager() {
    ConcurrentMapCacheManager cacheManager = new ConcurrentMapCacheManager() {

      @Override
      protected Cache createConcurrentMapCache(final String name) {
        return new ConcurrentMapCache(name,
            CacheBuilder
                .newBuilder().expireAfterWrite(300, TimeUnit.SECONDS)
                .maximumSize(1000).build().asMap(), false);
      }
    };

    return cacheManager;
  }


}
