package com.ascendcorp.aml.config.db;

import java.util.HashMap;
import java.util.Map;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@ConfigurationProperties(prefix = "datasource")
@RefreshScope
@Data
public class DataSourceProperties {

  private Map<String, DataSourceConfiguration> configurations = new HashMap<>();

  public boolean isConfigurationSupported(String key) {
    return configurations.keySet().contains(key);
  }
}
