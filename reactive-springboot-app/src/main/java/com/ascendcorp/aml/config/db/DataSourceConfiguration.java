package com.ascendcorp.aml.config.db;

import lombok.Data;

@Data
public class DataSourceConfiguration {

  private String username;

  private String password;

  private String databaseType;

  private Integer maximumPoolSize = 10;

  private String host;

  private Integer port;

  private String database;

  private String schema = "public";
}
