package com.ascendcorp.aml.config.cache;

import com.ascendcorp.aml.service.TenantIdHolder;
import java.lang.reflect.Method;
import org.springframework.cache.interceptor.SimpleKeyGenerator;
import reactor.core.publisher.Mono;

public class CustomKeyGenerator extends SimpleKeyGenerator {

  @Override
  public Mono<String> generate(Object target, Method method, Object... params) {
    return TenantIdHolder.getTenantId().map(tenantId -> tenantId.toString() + "_" + generateKey(params));
  }
}
