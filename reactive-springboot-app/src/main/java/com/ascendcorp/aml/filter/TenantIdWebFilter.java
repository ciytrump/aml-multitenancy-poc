package com.ascendcorp.aml.filter;

import com.ascendcorp.aml.service.TenantIdHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

@Slf4j
@Component
public class TenantIdWebFilter implements WebFilter {

  public static final String COUNTRY_CODE_HEADER = "country_code";

  @Override
  public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
    try {
      String tenantId = extractTenantIdIntoContext(exchange);

      if (StringUtils.hasText(tenantId)) {

        return chain.filter(exchange).subscriberContext(TenantIdHolder.withTenantId(tenantId));
      }
      return chain.filter(exchange);
    } finally {
      TenantIdHolder.clearContext();
    }
  }

  private String extractTenantIdIntoContext(ServerWebExchange exchange) {
    String tenantId = exchange.getRequest().getHeaders().getFirst(COUNTRY_CODE_HEADER);
    log.info("method: extractTenantIdIntoContext, tenantId: {}", tenantId);
    return tenantId;
  }
}
