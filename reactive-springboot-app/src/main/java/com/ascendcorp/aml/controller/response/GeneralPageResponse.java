package com.ascendcorp.aml.controller.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import java.io.Serializable;
import java.util.List;
import lombok.Data;

@Data
@JsonNaming(SnakeCaseStrategy.class)
public class GeneralPageResponse<T> implements Serializable {

  private static final long serialVersionUID = 1L;

  private ResponseStatus status;

  private List<T> data;

  private PageResponse page;

}
