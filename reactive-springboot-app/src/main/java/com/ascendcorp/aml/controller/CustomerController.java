package com.ascendcorp.aml.controller;

import com.ascendcorp.aml.controller.response.GeneralResponse;
import com.ascendcorp.aml.controller.response.ResponseFactory;
import com.ascendcorp.aml.dto.CustomerDto;
import com.ascendcorp.aml.service.CustomerService;
import java.util.Locale;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
public class CustomerController {

  private final CustomerService customerService;

  private final ResponseFactory responseFactory;

  @PostMapping("/customers")
  public Mono<GeneralResponse<Long>> createCustomer(
      @RequestBody CustomerDto request,
      @RequestHeader(value = "country_code", required = false) String countryCode,
      Locale locale) {
    return customerService.createCustomer(request).map(it -> responseFactory.success(it, locale));
    //    return responseFactory.success(, locale);
  }

  @GetMapping("/customers/{id}")
  public Mono<GeneralResponse<Object>> getCustomer(
      @PathVariable Long id,
      @RequestHeader(value = "country_code", required = false) String countryCode,
      Locale locale) {
    return customerService.getCustomer(id).map(it -> responseFactory.success(it, locale));
  }

  @DeleteMapping("/customers/{id}")
  public Mono<GeneralResponse<Object>> deleteCustomer(
      @PathVariable Long id,
      @RequestHeader(value = "country_code", required = false) String countryCode,
      Locale locale) {
    return customerService.deleteCustomer(id).map(it -> responseFactory.success(locale));
  }
}
