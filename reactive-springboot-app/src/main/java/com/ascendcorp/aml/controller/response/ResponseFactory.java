package com.ascendcorp.aml.controller.response;

import com.ascendcorp.aml.exception.ErrorCode;
import java.util.List;
import java.util.Locale;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ResponseFactory {

  private final MessageSource messageSource;

  public <T> GeneralResponse<T> success(T data, Locale locale) {
    GeneralResponse<T> responseObject = new GeneralResponse<>();
    ResponseStatus responseStatus =
        new ResponseStatus(
            ErrorCode.SUCCESS.getCode(),
            messageSource.getMessage(ErrorCode.SUCCESS.getCode(), null, locale));
    responseObject.setStatus(responseStatus);
    responseObject.setData(data);
    return responseObject;
  }

  public GeneralResponse<Object> success(Locale locale) {
    GeneralResponse<Object> responseObject = new GeneralResponse<>();
    ResponseStatus responseStatus =
        new ResponseStatus(
            ErrorCode.SUCCESS.getCode(),
            messageSource.getMessage(ErrorCode.SUCCESS.getCode(), null, locale));
    responseObject.setStatus(responseStatus);
    return responseObject;
  }

  public <T> GeneralPageResponse<T> success(List<T> data, PageResponse page, Locale locale) {
    GeneralPageResponse<T> responseObject = new GeneralPageResponse<>();
    ResponseStatus responseStatus =
        new ResponseStatus(
            ErrorCode.SUCCESS.getCode(),
            messageSource.getMessage(ErrorCode.SUCCESS.getCode(), null, locale));
    responseObject.setStatus(responseStatus);
    responseObject.setData(data);
    responseObject.setPage(page);
    return responseObject;
  }

  public GeneralResponse<Object> error(ErrorCode errorCode, Object[] params, Locale locale) {
    ResponseStatus responseStatus =
        new ResponseStatus(
            errorCode.getCode(), messageSource.getMessage(errorCode.getCode(), params, locale));
    GeneralResponse<Object> responseObject = new GeneralResponse<>();
    responseObject.setStatus(responseStatus);
    return responseObject;
  }

  public GeneralResponse<Object> error(
      ErrorCode errorCode, String messageCode, Object[] params, Locale locale) {
    ResponseStatus responseStatus =
        new ResponseStatus(
            errorCode.getCode(), messageSource.getMessage(messageCode, params, locale));
    GeneralResponse<Object> responseObject = new GeneralResponse<>();
    responseObject.setStatus(responseStatus);
    return responseObject;
  }
}
