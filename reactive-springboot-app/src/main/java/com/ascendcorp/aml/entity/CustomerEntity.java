package com.ascendcorp.aml.entity;

import java.sql.Timestamp;
import java.util.Date;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table("customer")
@Data
public class CustomerEntity {

  @Id
  @Column("id")
  private Long id;

  @Column("name")
  private String name;

  @Column("age")
  private Integer age;

  @Column("created_at")
  private Timestamp createdAt;
}
