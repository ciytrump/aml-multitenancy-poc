package com.ascendcorp.aml;

import brave.propagation.TraceContext;
import com.ascendcorp.aml.service.TenantIdHolder;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.util.context.Context;

@Slf4j
public class TestContext {
  public static void main(String[] args) {


//    Mono.just(1)
//        .doOnNext(it -> log.info("Item: {}", it))
//        .publishOn(Schedulers.boundedElastic())
//        .flatMap(
//            it ->
//                Mono.deferWithContext(
//                    c -> {
//                      c.stream()
//                          .forEach(
//                              cIt -> {
//                                log.info("k: {}, v: {}", cIt.getKey(), cIt.getValue());
//                              });
//                      return Mono.just(it);
//                    }))
//        .subscriberContext(Context.of("a", "A"))
//        .subscribe();

      Mono<Integer> monoI = Mono.just(1).subscriberContext(TenantIdHolder.withTenantId("a"));



      TenantIdHolder.getTenantId().doOnNext(it -> log.info("{}", it)).flatMap(id -> monoI).doOnNext(it -> log.info("{}", it)).subscribe();

  }
}
