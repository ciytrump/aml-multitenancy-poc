package com.ascendcorp.aml.dto;

import java.util.Date;
import lombok.Data;

@Data
public class CustomerDto {

  private Long id;

  private String name;

  private Integer age;

  private Date createdAt;
}
