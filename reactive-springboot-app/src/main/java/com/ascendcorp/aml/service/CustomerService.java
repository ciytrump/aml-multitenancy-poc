package com.ascendcorp.aml.service;

import com.ascendcorp.aml.dto.CustomerDto;
import com.ascendcorp.aml.entity.CustomerEntity;
import reactor.core.publisher.Mono;

public interface CustomerService {

  Mono<Long> createCustomer(CustomerDto request);

  Mono<CustomerEntity> getCustomer(Long id);

  Mono<Integer> deleteCustomer(Long id);
}
