package com.ascendcorp.aml.service;

import com.ascendcorp.aml.dto.CustomerDto;
import com.ascendcorp.aml.entity.CustomerEntity;
import com.ascendcorp.aml.exception.BusinessException;
import com.ascendcorp.aml.exception.ErrorCode;
import com.ascendcorp.aml.mapper.CustomerMapper;
import com.ascendcorp.aml.repository.CustomerRepository;
import java.sql.Timestamp;
import java.time.Duration;
import java.util.Date;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

@Service
@Slf4j
@RequiredArgsConstructor
public class StdCustomerService implements CustomerService {

  private final CustomerRepository customerRepository;

  private final CustomerMapper mapper;

  private int count = 0;

  @Override
  @Transactional
  public Mono<Long> createCustomer(CustomerDto request) {
    log.info("method: createCustomer, request: {}", request);

    CustomerEntity entity = mapper.toEntity(request);
    entity.setId(null);
    entity.setCreatedAt(new Timestamp(new Date().getTime()));

    return customerRepository
        .save(entity)
        .map(CustomerEntity::getId)
        .doOnSuccess(
            it -> {
              count++;
              if (count % 2 == 0) {
                log.error("method: createCustomer, test rollback transaction, count:{}", count);
                throw new BusinessException(ErrorCode.ERROR_COMMON_INVALID_REQUEST);
              }
            });
  }

  @Override
  @Transactional
  @Cacheable(value = "c", cacheManager = "test01", key = "#id")
  public Mono<CustomerEntity> getCustomer(Long id) {
    log.info("method: getCustomer, id: {}", id);
    return customerRepository
        .findById(id)
        .cache();
//        .filter(
//            it -> {
//              if (Objects.isNull(it)) {
//                throw new BusinessException(ErrorCode.ENTITY_NOT_FOUND, new Object[] {id});
//              }
//              return true;
//            })
//        .map(mapper::toDto);
  }

  @Override
  @Transactional
  public Mono<Integer> deleteCustomer(Long id) {
    log.info("method: deleteCustomer, id: {}", id);
    return customerRepository
        .findById(id)
        .filter(
            it -> {
              if (Objects.isNull(it)) {
                throw new BusinessException(ErrorCode.ENTITY_NOT_FOUND, new Object[] {id});
              }
              return true;
            })
        .flatMap(customerRepository::delete)
        .thenReturn(1);
  }
}
